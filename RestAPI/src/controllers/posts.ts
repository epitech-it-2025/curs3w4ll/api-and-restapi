import { Request, Response, NextFunction } from "express";
import axios, { AxiosResponse } from "axios";

interface Post {
  userId: Number;
  id: Number;
  title: String;
  body: String;
}

export async function getPosts(
  _req: Request,
  res: Response,
  _next: NextFunction
) {
  const result: AxiosResponse = await axios.get(
    "https://jsonplaceholder.typicode.com/posts"
  );
  const posts: [Post] = result.data;

  return res.status(200).json({
    message: posts,
  });
}

export async function getPost(
  req: Request,
  res: Response,
  _next: NextFunction
) {
  const id: String = req.params.id;

  const result: AxiosResponse = await axios.get(
    `https://jsonplaceholder.typicode.com/posts/${id}`
  );
  const post: Post = result.data;

  return res.status(200).json({
    message: post,
  });
}

export async function updatePost(
  req: Request,
  res: Response,
  _next: NextFunction
) {
  const id: String = req.params.id;
  const title: String = req.body.title ?? null;
  const body: String = req.body.body ?? null;

  const response: AxiosResponse = await axios.put(
    `https://jsonplaceholder.typicode.com/posts/${id}`,
    {
      ...(title && { title }),
      ...(body && { body }),
    }
  );

  return res.status(200).json({
    message: response.data,
  });
}

export async function deletePost(
  req: Request,
  res: Response,
  _next: NextFunction
) {
  const id: String = req.params.id;

  const response: AxiosResponse = await axios.delete(
    `https://jsonplaceholder.typicode.com/posts/${id}`
  );

  return res.status(200).json({
    message: "Post deleted successfully",
  });
}

export async function addPost(
  req: Request,
  res: Response,
  _next: NextFunction
) {
  const title: String = req.body.title;
  const body: String = req.body.body;

  const response: AxiosResponse = await axios.post(
    "https://jsonplaceholder.typicode.com/posts",
    {
      title,
      body,
    }
  );

  return res.status(200).json({
    message: response.data,
  });
}

export default { getPosts, getPost, updatePost, deletePost, addPost };
