# API VS RestAPI

## What is an API ?

<details>
<summary>Do popular services like github, gitlab, spotify etc have an API ?</summary>
Yes, this is commons API, web APIs.
</details>

<details>
<summary>Do the linux kernel have an API ?</summary>
Yes, the linux kernel expose an API to interact with it, syscalls are a main part of the kernel API for example.
</details>

<details>
<summary>Do your keyboard is used through an API ?</summary>
Yes, the `/dev/USB` let you interact with the your keyboard through an API.
</details>

<details>
<summary>Do the Windows system have an API ?</summary>
Yes, creating windows, service, hanling cursor, native buttons... can be done using the Windows API.
</details>

<details>
<summary>How do you use your monitor and screens ?</summary>
Through an API.
</details>

<details>
<summary>Can your graphical server (X or Wayland) be used through an API ?</summary>
Yes.
</details>

<details>
<summary>Is the standard libC is an API ?</summary>
Yes, this is another kind of API through functions and types prototypes.
</details>

---

You will think that everything is an API then ?  
Well, it's a bit true.

A lot of things have APIs and can be used through APIs.

**When at least two programs communicate, then their is an API.**

Even when you do a group project, you closely make an API, you make functions that are prototypes for other members and they don't really know what happens inside.

---

An **API**, or *Application Programming Interface* is a set of functions and structures used as frontage exposed to offer functionnalities to other apps.

An API is an interface that permits to differents services to exchange datas in the form of contracts defined and known from the two side.
We talk about API when an application try to communicate with another one with structured messages and with constraints established by the contacted application. We then say that this contacted application "expose" an API.

# What is a RestAPI

A RestAPI is a web API that use a norme to standardize the communication between two computers.  
In 2000s, an experts group leaded by Roy Fielding will invent RestAPIs and changed the look of APIs forever.

RestAPI means *Representational State Transfer*, Rest comes with a set of rules to follow. Even is their is a lot of rules to respect, they are universal and permit to the RestAPI to make integrations easy.

In 2004, [Flickr](https://www.flickr.com/) deliver it's first RestAPI publicly and in 2006, [Amazon](https://www.amazon.fr) did the same.

RestAPI define a set of rules and constraints:
- **Client-server architecture**: A REST architecture is composed of clients, servers and resources and use HTTP Protocols (POST, PUT, PATCH, GET, DELETE)
- **Stateless server**: The content of the client is **never** stored on the serveur between requests.
- **Cacheable**: Cache memory allow to skip some of the interactions between clients and servers, this can save time and resources.
- **Layered system**: Supplementary layers can be implemented to improve cache, security, scalability.
- **On demand code**: A server can extend clients functionnalities by sending it som code
- **Uniform interface**: This is the biggest constraint about creating RestAPI, and it comes with four big aspects:
    - **Resources identification in requests**: Resources must be identified and separated from the representation when sended to client.
    - **Resources manipulation through representations**: Clients receive resources through files. This files must contains enough resources to modify and delete informations.
    - **Descriptif messages**: Clients must received enough informations to know how to decode and read datas in payloads.
    - **Hypermedia**: Once the client access a resource, it must know every others actions he can do by hyperlink.

# How to build a RestAPI

You have an example of an [ExpressJS](https://expressjs.com/fr/) RestAPI [here](/RestAPI).

You have many tutos to know how to build a RestAPI, search a bit and you will find some doc.  
The RestAPI is the most common WebAPI, almost everything you will see about a WebAPI is in fact something about a RestAPI.

Tutos:
- [ExpressJS](https://www.section.io/engineering-education/how-to-create-a-simple-rest-api-using-typescript-and-nodejs/)
- [.NET](https://learn.microsoft.com/en-us/aspnet/web-api/overview/older-versions/build-restful-apis-with-aspnet-web-api)
- [Django](https://blog.logrocket.com/django-rest-framework-create-api/)
